{ nixpkgs ? import ./pinned-nixpkgs.nix {}
, ghc
}:

with nixpkgs;

haskell.lib.buildStackProject {
  inherit ghc;
  name = "obi-tiny-godot";
  buildInputs = [];
}


