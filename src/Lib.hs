{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# OPTIONS_GHC -fno-warn-incomplete-uni-patterns #-}
module Lib
  ( exports
  )
where

import           Control.Monad
import qualified Data.Text                     as T
import           Data.Function                            ( (&) )
import           Data.Typeable
import           Godot
import           Godot.Gdnative.Types
import qualified Godot.Gdnative.Internal.Api   as Api
import qualified Foreign.C                     as Foreign
import           Linear.V2
import           Linear.Vector


exports :: GdnativeHandle -> IO ()
exports desc = do
  registerClass $ RegClass desc $ classInit @Main
  registerClass $ RegClass desc $ classInit @TreeShort


data Main = Main { _mBase :: GodotNode }

instance HasBaseClass Main where
  type BaseClass Main = GodotNode
  super = _mBase
instance NativeScript Main where
  classInit base = return $ Main base
  classMethods = []


data TreeShort = TreeShort { _tsBase :: GodotSprite }
instance HasBaseClass TreeShort where
  type BaseClass TreeShort = GodotSprite
  super = _tsBase
instance NativeScript TreeShort where
  classInit base = return $ TreeShort base
  classMethods =
    [ func NoRPC "_ready" $ \_self [] ->
        Api.godot_print =<< toLowLevel "Hi"

    , func NoRPC "_process" $ \self [deltaVt] -> do
          delta    <- fromGodotVariant deltaVt
          Just inp <- getSingleton @GodotInput
          actions self delta inp
    ]


data EnemyBug = EnemyBug { _ebBase :: GodotSprite, _ebPlayer :: TVar (Maybe GodotSprite) }
instance HasBaseClass EnemyBug where
  type BaseClass EnemyBug = GodotSprite
  super = _ebBase
instance NativeScript EnemyBug where
  classInit base = EnemyBug base <$> newTVarIO Nothing
  classMethods = []
    {-
     -[ func NoRPC "_physics_process" $ \self [deltaVt] -> do
     -    readTVarIO (_ebPlayer self) >>= \case
     -      Just player ->
     -]
     -}

{-# OPTIONS_GHC -fwarn-incomplete-uni-patterns #-}


data Action
  = MoveUp
  | MoveDown
  | MoveLeft
  | MoveRight


actions :: TreeShort -> Float -> GodotInput -> IO ()
actions self delta inp = do
  mvDirOn MoveUp $ V2 0 (-1)
  mvDirOn MoveDown $ V2 0 1
  mvDirOn MoveLeft $ V2 (-1) 0
  mvDirOn MoveRight $ V2 1 0
 where
  mvDirOn act dir =
    let move v = get_position self >>= onLowLevel (v ^+^) >>= set_position self
        dist = delta * 100
    in  onAction act inp $ move $ dir ^* dist


isAction :: Action -> GodotInput -> IO Bool
isAction action inp = case action of
  MoveUp    -> iap "ui_up"
  MoveDown  -> iap "ui_down"
  MoveLeft  -> iap "ui_left"
  MoveRight -> iap "ui_right"
  where iap str = is_action_pressed inp =<< toLowLevel str


onAction :: Action -> GodotInput -> IO () -> IO ()
onAction action inp io = do
  b <- isAction action inp
  when b io



--- UTIL


onLowLevel
  :: (GodotFFI low0 high0, GodotFFI low1 high1)
  => (high0 -> high1)
  -> low0
  -> IO low1
onLowLevel f low = toLowLevel =<< f <$> fromLowLevel low


getSingleton
  :: forall a . (Typeable a, AsVariant a, GodotObject :< a) => IO (Maybe a)
getSingleton = do
  ge <- getEngine
  let name = nameOf @a
  nameLL <- toLowLevel name
  tryCast =<< get_singleton ge nameLL


getEngine :: IO Godot_Engine
getEngine =
  Api.godot_global_get_singleton
    &   Foreign.withCString (T.unpack "Engine")
    >>= \o -> tryCast o >>= \mGE -> case mGE of
          Just ge -> return ge
          Nothing ->
            (get_class o :: IO GodotString)
              >>= fromLowLevel
              >>= error
              .   ("Couldn't get Engine singleton :( got: " ++)
              .   T.unpack


-- newInstance :: forall a . (Typeable a, AsVariant a, GodotObject :< a) => IO a
-- newInstance = do
--   Just cdb <- getSingleton @Godot_ClassDB
--   obj      <-
--     fromGodotVariant =<< instance' cdb =<< toLowLevel (nameOf @a) :: IO
--       GodotObject
--   tryCast obj >>= maybe (error "Failed to instance class") return
